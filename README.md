# README

## Dev Setup Notes

## Database Configuration

Rails now supports the commonly used DATABASE_URL environment variable. If
you've installed PostgreSQL from Homebrew you can easily setup your db by
adding the following to your `~/.bashrc` or `~/.zshrc`:

```
export DATABASE_URL="postgresql://${USER}@localhost"
# Don't forget to source your .bashrc or .zshrc file after you save
```

Now you can easily create the databases & initial data needed with:

```
bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
# Be patient, populating every city & zip code in the country takes a while...
```

### macOS Sierra:

For rmagick to install properly you'll need to run the following:

```
brew install pkg-config
brew install imagemagick@6
brew link imagemagick@6 --force
# Now you can install rmagick
gem install rmagick
```
