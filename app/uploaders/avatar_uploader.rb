class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  include CarrierWave::ImageOptimizer

  storage :file

  def store_dir
    "public/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :full do
    process :resize_to_fit => [800,800]
    process :convert => 'jpg'
    process optimize: [{ quality: 60 }]
  end
  version :medium do
    process :resize_to_fill => [300,300]
    process :convert => 'jpg'
    process optimize: [{ quality: 60 }]
  end
  version :small do
    process :resize_to_fill => [100, 100]
    process :convert => 'jpg'
    process optimize: [{ quality: 60 }]
  end
  version :thumb do
    process :resize_to_fill => [50, 50]
    process :convert => 'png'
    process :optimize
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png tif tiff pdf)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  def content_type_whitelist
    /image\//
  end

end
