require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception

  def resource_params
    params.require(resource_name).permit(*permitted_resource_params)
  end

end
