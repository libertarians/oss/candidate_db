class Admin::CandidatesController < Admin::AdminController
  before_action :set_candidate, only: [:show, :edit, :update, :destory]

  respond_to :html, :json

  def index
    @candidates = Candidate.order(:lastname)
    respond_with @candidates
  end

  def show
    respond_with @candidate
  end

  def new
    @candidate = Candidate.new
    respond_with @candidate
  end

  def create
    @candidate = Candidate.new(resource_params)

    respond_to do |format|
      if @candidate.save
        format.html { redirect_to [:admin, :candidates], notice: 'Candidate was successfully created' }
        format.json { render json: @candidate, status: :created, location: @candidate }
      else
        format.html { render action: "new" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @candidate.update(resource_params)
        format.html { redirect_to [:admin, :candidates], notice: 'Candidate was successfully updated' }
        format.json { render json: @candidate, status: :updated, location: @candidate }
      else
        format.html { render action: "edit" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @candidate.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  private

  def set_candidate
    @candidate = Candidate.find(params[:id])
  end

  def resource_name
    :candidate
  end

  def permitted_resource_params
    [:firstname, :lastname, :suffix, {avatars: []}]
  end

end
