class Admin::OfficesController < Admin::AdminController
  before_action :set_office, only: [:show, :edit, :update, :destory]

  respond_to :html, :json

  def index
    @offices = Office.order(:name)
    respond_with @offices
  end

  def show
    respond_with @office
  end

  def new
    @office = Office.new
    respond_with @office
  end

  def create
    @office = Office.new(resource_params)

    respond_to do |format|
      if @office.save
        format.html { redirect_to [:admin, :offices], notice: 'Office was successfully created' }
        format.json { render json: @office, status: :created, location: @office }
      else
        format.html { render action: "new" }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @office.update(resource_params)
        format.html { redirect_to [:admin, :offices], notice: 'Office was successfully updated' }
        format.json { render json: @office, status: :updated, location: @office }
      else
        format.html { render action: "edit" }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @office.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  private

  def set_office
    @office = Office.find(params[:id])
  end

  def resource_name
    :office
  end

  def permitted_resource_params
    [:name, :description, :locatable_type, :locatable_id, :locatable_global_id]
  end

end
