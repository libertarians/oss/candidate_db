class Admin::CampaignsController < Admin::AdminController
  before_action :set_campaign, only: [:show, :edit, :update, :destory]

  respond_to :html, :json

  def index
    @campaigns = Campaign.order(:year)
    respond_with @campaigns
  end

  def show
    respond_with @campaign
  end

  def new
    @campaign = Campaign.new
    respond_with @campaign
  end

  def create
    @campaign = Campaign.new(resource_params)

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to [:admin, :campaigns], notice: 'Campaign was successfully created' }
        format.json { render json: @campaign, status: :created, location: @campaign }
      else
        format.html { render action: "new" }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @campaign.update(resource_params)
        format.html { redirect_to [:admin, :campaigns], notice: 'Campaign was successfully updated' }
        format.json { render json: @campaign, status: :updated, location: @campaign }
      else
        format.html { render action: "edit" }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @campaign.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  private

  def set_campaign
    @campaign = Campaign.find(params[:id])
  end

  def resource_name
    :campaign
  end

  def permitted_resource_params
    [:office_id, :year, :url, :candidate_statement, :candidate_id, :instagram, :facebook]
  end

end
