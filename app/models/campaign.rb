class Campaign < ApplicationRecord
  belongs_to :office
  belongs_to :candidate

  validates :office_id, presence: true
  validates :candidate_id, presence: true
  validates :year, presence: true

  # TODO: This should move to a decorator
  def title
    "#{self.office.name} #{self.year.stamp("1999")} — #{self.candidate.fullname}"
  end
end
