class Candidate < ApplicationRecord
  mount_uploader :avatar, AvatarUploader

  has_many :campaigns
  has_many :offices, through: :campaigns

  validates :firstname,
            presence: true
  validates :lastname,
            presence: true

  def fullname
    "#{firstname} #{lastname} #{suffix}"
  end

end
