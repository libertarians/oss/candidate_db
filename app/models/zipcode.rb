class Zipcode < ApplicationRecord
  belongs_to :city, inverse_of: :zipcodes

  validates :city, presence: true
  validates :code, presence: true, uniqueness: {case_sensitive: false}
end
