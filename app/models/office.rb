class Office < ApplicationRecord
  has_many :campaigns
  has_many :candidates, through: :campaigns
  belongs_to :locatable, polymorphic: true

  validates :name, presence: true
  validates :locatable_id, presence: true
  validates :locatable_type, presence: true
end
