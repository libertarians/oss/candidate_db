class City < ApplicationRecord
  include GlobalID::Identification

  has_many :office, as: :locatable

  belongs_to :county, inverse_of: :cities
  has_many :zipcodes, inverse_of: :cities

  validates :county, presence: true
  validates :name,   presence: true, uniqueness: {case_sensitive: false, scope: :county_id}
end
