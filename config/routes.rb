Rails.application.routes.draw do
  root to: 'admin/admin#index'

  namespace :admin do
    get '/' => 'admin#index'
    resources :campaigns
    resources :candidates
    resources :offices
  end
end
