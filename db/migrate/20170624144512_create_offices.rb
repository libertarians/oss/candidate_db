class CreateOffices < ActiveRecord::Migration[5.1]
  def change
    create_table :offices do |t|
      t.string :name, null: false
      t.string :jurisdiction, null: false
      t.text :description

      t.timestamps
    end
  end
end
