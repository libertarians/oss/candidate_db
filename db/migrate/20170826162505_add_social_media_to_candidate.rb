class AddSocialMediaToCandidate < ActiveRecord::Migration[5.1]
  def change
    add_column :candidates, :instagram, :string
    add_column :candidates, :linkedin, :string
    add_column :candidates, :facebook, :string
    add_column :candidates, :twitter, :string
  end
end
