class CreateCampaigns < ActiveRecord::Migration[5.1]
  def change
    create_table :campaigns do |t|
      t.integer :candidate_id, null: false
      t.integer :office_id, null: false
      t.integer :year, null: false
      t.string :url
      t.text :candidate_statement

      t.timestamps
    end
  end
end
