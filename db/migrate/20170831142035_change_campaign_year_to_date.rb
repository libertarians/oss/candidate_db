class ChangeCampaignYearToDate < ActiveRecord::Migration[5.1]
  def change
    remove_column :campaigns, :year
    add_column :campaigns, :year, :date
  end
end
