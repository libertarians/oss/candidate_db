class CreateCandidates < ActiveRecord::Migration[5.1]
  def change
    create_table :candidates do |t|
      t.string :firstname, null: false
      t.string :lastname, null: false
      t.string :suffix

      t.timestamps
    end
  end
end
