class ChangeOfficeJurisdictionToPolymorphic < ActiveRecord::Migration[5.1]
  def change
    remove_column :offices, :jurisdiction
    add_column :offices, :locatable_id, :integer
    add_column :offices, :locatable_type, :string
    add_index :offices, [:locatable_type, :locatable_id]
  end
end
