class AddSocialMediaToCampaign < ActiveRecord::Migration[5.1]
  def change
    add_column :campaigns, :instagram, :string
    add_column :campaigns, :facebook, :string
    add_column :campaigns, :twitter, :string
  end
end
