class CreateCounties < ActiveRecord::Migration[5.1]
  def change
    create_table :counties do |t|
      t.integer :state_id, null: false
      t.string :name, null: false
      t.timestamps
    end
  end
end
