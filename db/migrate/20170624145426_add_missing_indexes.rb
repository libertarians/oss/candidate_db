class AddMissingIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :campaigns, :candidate_id
    add_index :campaigns, :office_id
    add_index :cities, :county_id
    add_index :counties, :state_id
    add_index :zipcodes, :city_id
  end
end
