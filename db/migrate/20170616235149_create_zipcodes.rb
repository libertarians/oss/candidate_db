class CreateZipcodes < ActiveRecord::Migration[5.1]
  def change
    create_table :zipcodes do |t|
      t.integer :state_id, null: false
      t.integer :county_id, null: false
      t.integer :city_id, null: false
      t.integer :code, null: false
      t.timestamps
    end
  end
end
