# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170831142035) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "campaigns", force: :cascade do |t|
    t.integer "candidate_id", null: false
    t.integer "office_id", null: false
    t.string "url"
    t.text "candidate_statement"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "instagram"
    t.string "facebook"
    t.string "twitter"
    t.date "year"
    t.index ["candidate_id"], name: "index_campaigns_on_candidate_id"
    t.index ["office_id"], name: "index_campaigns_on_office_id"
  end

  create_table "candidates", force: :cascade do |t|
    t.string "firstname", null: false
    t.string "lastname", null: false
    t.string "suffix"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar"
    t.string "instagram"
    t.string "linkedin"
    t.string "facebook"
    t.string "twitter"
  end

  create_table "cities", force: :cascade do |t|
    t.integer "county_id", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["county_id"], name: "index_cities_on_county_id"
  end

  create_table "counties", force: :cascade do |t|
    t.integer "state_id", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_counties_on_state_id"
  end

  create_table "offices", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "locatable_id"
    t.string "locatable_type"
    t.index ["locatable_type", "locatable_id"], name: "index_offices_on_locatable_type_and_locatable_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "two_digit_code", null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "zipcodes", force: :cascade do |t|
    t.integer "state_id", null: false
    t.integer "county_id", null: false
    t.integer "city_id", null: false
    t.integer "code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_zipcodes_on_city_id"
  end

end
