FactoryGirl.define do
  factory :county do
    association :state
    name { Faker::Address.city }
  end
end
