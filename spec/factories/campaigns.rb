FactoryGirl.define do
  factory :campaign do
    association :candidate, factory: :candidate
    association :office, factory: :office
    year "2017-06-24"
    url "MyString"
    candidate_statement "MyText"
  end
end
