FactoryGirl.define do
  factory :office do
    name { Faker::Name.title }
    association :locatable, factory: :city
    description { Faker::Lorem.paragraph(sentence_count = 3) }
  end
end
