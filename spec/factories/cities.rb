FactoryGirl.define do
  factory :city do
    association :county
    name { Faker::Address.city }
  end
end
