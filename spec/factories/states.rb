FactoryGirl.define do
  factory :state do
    two_digit_code { Faker::Address.state_abbr }
    name { Faker::Address.state }
  end
end
