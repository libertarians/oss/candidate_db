FactoryGirl.define do
  factory :candidate do
    firstname { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    suffix { Faker::Name.suffix }
  end
end
