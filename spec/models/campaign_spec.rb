require 'rails_helper'

RSpec.describe Campaign, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:campaign)).to be_valid
  end

  it "is invalid without a office association" do
    expect(FactoryGirl.build(:campaign, office: nil)).to_not be_valid
  end

  it "is invalid without a candidate association" do
    expect(FactoryGirl.build(:campaign, candidate: nil)).to_not be_valid
  end

  it "is invalid without a year" do
    expect(FactoryGirl.build(:campaign, year: nil)).to_not be_valid
  end
end
