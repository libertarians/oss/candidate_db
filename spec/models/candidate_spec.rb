require 'rails_helper'

RSpec.describe Candidate, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:candidate)).to be_valid
  end

  it "is invalid without a firstname" do
    expect(FactoryGirl.build(:candidate, firstname: nil)).to_not be_valid
  end

  it "is invalid without a lastname" do
    expect(FactoryGirl.build(:candidate, lastname: nil)).to_not be_valid
  end

  it "is valid without a suffix" do
    expect(FactoryGirl.build(:candidate, suffix: nil)).to be_valid
  end
end
