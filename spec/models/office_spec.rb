require 'rails_helper'

RSpec.describe Office, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:office)).to be_valid
  end

  it "is invalid without a name" do
    expect(FactoryGirl.build(:office, name: nil)).to_not be_valid
  end

  it "is invalid without a location" do
    expect(FactoryGirl.build(:office, locatable: nil)).to_not be_valid
  end
end
